package avng0;

import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
/**
 *
 * @author German_Aguirre
 */
public class Funcion2 extends CyclicBehaviour {

    public int control = 0;
    public double Caudal = 0;
    public double[] aux = new double[2];
    public String[] auxs = new String[2];
    private final Random rnd = new Random();
    private double geo;
    ACLMessage msj = new ACLMessage(ACLMessage.REQUEST);
    AID AVNG3 = new AID("AVNG3", AID.ISLOCALNAME);
    
    public double FNatural() throws IOException, BiffException {
        double v = leer(0, 1);
        double a = leer(1, 1);
        return v * a * rnd.nextDouble();
    }

    public double FAutonoma(double memoria) throws IOException, BiffException {

        double a = Double.valueOf(leer(1, 1));
        double p = Double.valueOf(leer(2, 1));
        //double aut=0;
        int size = sizefile() - 1;
        for (int i = 6; i < size; i++) {
            if (memoria != 0) {

                memoria = (a * Math.pow(p, 1.0 / 3.0) * Double.valueOf(leer(i, 1))) + memoria;
                
            } else {
                memoria = (a * Math.pow(p, 1.0 / 3.0) * Double.valueOf(leer(i, 1)));

            }

        }
        return memoria;
    }
 public int sizefile() throws IOException, BiffException
    {
        Workbook workbook; 
        workbook = Workbook.getWorkbook(new File("D:/DataSet3.xls")); //Pasamos el excel que vamos a leer
        Sheet sheet = workbook.getSheet(0);
    return sheet.getColumns();
    }
        public double FGeografica(double memoria) {
        geo = (Math.pow(memoria, 1.0 / 3.0)) * 0.0000000898311174991017;

        return geo;
    }

    public double converdatos(String nom) {
        double aux = 0;
        char c[] = nom.toCharArray();
        String num = "";
        for (int i = 0; i < c.length; i++) {
            if (c[i] == ',') {
                c[i] = '.';
            }
            num += c[i];
        }
        aux = Double.parseDouble(num);

        return aux;
    }

    public double FGAmbiental() {
        return 0;
    }

    public double leer(int col, int fil) throws IOException, BiffException {
        Workbook workbook;
        double ret = 0;
        try {
            workbook = Workbook.getWorkbook(new File("D:/DataSet3.xls")); //Pasamos el excel que vamos a leer
            Sheet sheet = workbook.getSheet(0); //Seleccionamos la hoja que vamos a leer
            String nombre;
            nombre = sheet.getCell(col, fil).getContents();//setear la celda leida a nombre
            ret = converdatos(nombre);
            return ret;

        } catch (IOException | BiffException ex) {
            Logger.getLogger(Funcion1.class
                    .getName()).log(Level.SEVERE, null, ex);

            return 0;
        }

    }
//Acciones del agente inteligente para enviar los datos a AVGN3
    @Override
    public void action(){
     try {
            ACLMessage AVNG1Env = myAgent.blockingReceive();
                if (Double.valueOf(leer(4, 2)) == 0) {      
                System.out.println(" " + myAgent.getLocalName() + " ");
                    if (AVNG1Env != null && "AVNG1".equals(AVNG1Env.getSender().getLocalName())) {
                    auxs = (String[]) AVNG1Env.getContentObject();
                    System.out.println("Mem recibida Izq" + auxs[0]);
                    aux[0] = FNatural();
                    auxs[0] = Double.toString(aux[0]);
                    System.out.println("Mem Natural Izq " + Double.toString(aux[0]));
                    System.out.println("GeoNat Izq:" + Double.toString(FGeografica(aux[0])));
                    } else {
                    block();
                    }             
                } else {

                    if (AVNG1Env != null && "AVNG1".equals(AVNG1Env.getSender().getLocalName())) {
                    auxs = (String[]) AVNG1Env.getContentObject();
                    aux[0] = Double.parseDouble(auxs[0]);
                    System.out.println(" " + myAgent.getLocalName() + " ");
                    System.out.println("Memoria recibida del lado Izq" + auxs[0]);
                    aux[0] = FAutonoma(aux[0]);
                    auxs[0] = Double.toString(aux[0]);
                    System.out.println("Memoria  Autonoma Izq " + Double.toString(aux[0]));
                    System.out.println("GeoAut Izq:" + Double.toString(FGeografica(aux[0])));
                    System.out.println("der" + aux[1]);
                    } else {
                    block();
                    }
                }   
                if (Double.valueOf(leer(5, 2)) == 0) {

                    if (AVNG1Env != null && "AVNG1".equals(AVNG1Env.getSender().getLocalName())) {
                    auxs = (String[]) AVNG1Env.getContentObject();
                    System.out.println("Mem recibida Der" + Double.toString(aux[1]));
                    aux[1] = FNatural();
                    auxs[1] = Double.toString(aux[1]);
                    System.out.println("Mem Natural Der : " + Double.toString(aux[1]));
                    System.out.println("GeoNat Der es : " + Double.toString(FGeografica(aux[1])));

                    } else {
                    block();
                    }
              
                } else {    

                if (AVNG1Env != null && "AVNG1".equals(AVNG1Env.getSender().getLocalName())) {
                    auxs = (String[]) AVNG1Env.getContentObject();
                    aux[1] = Double.parseDouble(auxs[1]);
                    System.out.println("Mem Recibida Der " + auxs[1]);
                    aux[1] = FAutonoma(aux[1]);
                    auxs[1] = Double.toString(aux[1]);
                    System.out.println("Mem Autonoma Der :  " + Double.toString(aux[1]));
                    System.out.println("GeoAut Der es : " + Double.toString(FGeografica(aux[1])));
                } else {
                    block();
                }
            }
            msj.addReceiver(AVNG3);
            msj.setContentObject(auxs);
            myAgent.send(msj);
            System.out.println("Mensaje Enviado:" + auxs[0]+"Izq" + " || " + auxs[1]+"Der");
            System.out.println("prueba end AVGN2");
            auxs = null;
            block();

        } catch (IOException | BiffException ex) {
            Logger.getLogger(Funcion2.class
                    .getName()).log(Level.SEVERE, null, ex);
        } catch (UnreadableException ex) {
            Logger.getLogger(Funcion2.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

    }

}

