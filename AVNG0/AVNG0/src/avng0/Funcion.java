/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package avng0;

/**
 *
 * @author Usuario
 */
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

/**
 *
 * @author German_Aguirre
 */
public class Funcion extends CyclicBehaviour {

    public int control = 0;
    public double Caudal = 0;
    public double[] aux = new double[2];
    public String[] auxs = new String[2];
    private final Random rnd = new Random();
    private double geo;
    ACLMessage msj = new ACLMessage(ACLMessage.REQUEST);
    AID AVNG1 = new AID();

    public double FNatural() throws IOException, BiffException {
        double v = leer(0, 0);
        double a = leer(1, 0);
        return v * a * rnd.nextDouble();
    }

    public double FAutonoma(double memoria) throws IOException, BiffException {

        double a = Double.valueOf(leer(1, 0));
        double p = Double.valueOf(leer(2, 0));
        //double aut=0;
        int size = sizefile() - 1;
        for (int i = 6; i < size; i++) {
            if (memoria != 0) {

               
                memoria = (a * Math.pow(p, 1.0 / 3.0) * Double.valueOf(leer(i, 9))) + memoria;
                
            } else {
                memoria = (a * Math.pow(p, 1.0 / 3.0) * Double.valueOf(leer(i, 9)));

            }

        }
        return memoria;
    }

    public int sizefile() throws IOException, BiffException {
        Workbook workbook;
        workbook = Workbook.getWorkbook(new File("D:/DataSet3.xls")); //Pasamos el excel que vamos a leer
        Sheet sheet = workbook.getSheet(0);
        return sheet.getColumns();
    }

    public double FGeografica(double memoria) {
        geo = (Math.pow(memoria, 1.0 / 3.0)) * 0.0000000898311174991017;

        return geo;
    }

    public double converdatos(String nom) {
        double aux = 0;
        char c[] = nom.toCharArray();
        String num = "";
        for (int i = 0; i < c.length; i++) {
            if (c[i] == ',') {
                c[i] = '.';
            }
            num += c[i];
        }
        aux = Double.parseDouble(num);

        return aux;
    }

    public double FGAmbiental() {
        return 0;
    }

    public double leer(int col, int fil) throws IOException, BiffException {
        Workbook workbook;
        double ret = 0;
        try {
            workbook = Workbook.getWorkbook(new File("D:/DataSet3.xls")); //Pasamos el excel que vamos a leer
            Sheet sheet = workbook.getSheet(0); //Seleccionamos la hoja que vamos a leer
            String nombre;
            nombre = sheet.getCell(col, fil).getContents();//setear la celda leida a nombre
            ret = converdatos(nombre);
            return ret;

        } catch (IOException | BiffException ex) {
            Logger.getLogger(Funcion.class
                    .getName()).log(Level.SEVERE, null, ex);

            return 0;
        }

    }
//Aciones del agente AVNG0
    @Override
    public void action() {
        AVNG1.setLocalName("AVNG1");
        try {
            if (Double.valueOf(leer(4, 0)) == 0) {
                System.out.println(" " + myAgent.getLocalName() + " ");
                aux[0] = FNatural();
                auxs[0] = Double.toString(aux[0]);
                System.out.println("Natural Izq : " + Double.toString(aux[0]));
                System.out.println("GeoNat  Izq : " + Double.toString(FGeografica(aux[0])));
            } else {
                System.out.println(" " + myAgent.getLocalName() + " ");
                aux[0] = FAutonoma(aux[0]);
                System.out.println(" prueba aux"+aux[0]);
                auxs[0] = Double.toString(aux[0]);
                System.out.println("prueba aux double"+ auxs[0]);
                System.out.println("Autonoma Izq :" + Double.toString(aux[0]));
                System.out.println("GeoAut  Izq  :" + Double.toString(FGeografica(aux[0])));
            }
            if (Double.valueOf(leer(5, 0)) == 0) {
                aux[1] = FNatural();
                auxs[1] = Double.toString(aux[1]);
                System.out.println("Natural Der : " + Double.toString(aux[1]));
                System.out.println("GeoNat  Der : " + Double.toString(FGeografica(aux[1])));
            } else {
                aux[1] = FAutonoma(aux[0]);
                auxs[1] = Double.toString(aux[1]);
                System.out.println("Autonoma Der : " + Double.toString(aux[1]));
                System.out.println("GeoAut  Der : " + Double.toString(FGeografica(aux[1])));
            }

            msj.addReceiver(AVNG1);
            msj.setContentObject(auxs);
            myAgent.send(msj);
            System.out.println("Mensaje Enviado:" + auxs[0]+"Izq" + " || " + auxs[1]+"Der");
            System.out.println("prueba end AVGN0");
            auxs = null ;
            block();
        } catch (IOException | BiffException ex) {
            Logger.getLogger(Funcion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
